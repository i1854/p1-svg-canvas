console.log("HEllo WolWD");

let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");
//background
function drawBackground() {
  ctx.fillStyle = "#2a9df4";
  ctx.fillRect(0, 0, 300, 300);
}

//house
let housex = 30;
let housey = 0;
let housew = 80
let househ = 80

function houseDraw() {
  //  requestAnimationFrame(this.draw);
  ctx.fillStyle = "orange";
  ctx.fillRect(housex, housey, housew, househ)
}

function houseCheck(x, y) {
  if (x >= housex && x <= housex + housew && y >= housey && y <= housey + househ) {
    if (housey >= 180) {
      housey = 0;
    }
    return 1;
  } else {
    return 0;
  }
}

function houseMove() {
  if (housey != 180) {
    requestAnimationFrame(houseMove);
    ctx.clearRect(housex, housey, housew, househ);
    ctx.fillStyle = "orange";
    ctx.fillRect(housex, housey, housew, househ);
    housey += 1;
  }
}


//window1
let window1x = 80;
let window1y = 160;
let window1w = 20;
let window1h = 20;

function window1Draw() {
  ctx.fillStyle = "white";
  ctx.fillRect(window1x, window1y, window1w, window1h);
}

function window1Check(x, y) {
  if (x >= window1x && x <= window1x + window1w && y >= window1y && y <= window1y + window1h) {
    if (window1y >= 190) {
      window1y = 160;
    }
    return 1;
  } else {
    return 0;
  }
}

function window1Move() {
  if (window1y != 190) {
    requestAnimationFrame(window1Move);
    ctx.clearRect(window1x, window1y, window1w, window1h);
    ctx.fillStyle = "white";
    window1y += 0.25;
  }
}

//window2
let window2x = 200;
let window2y = 190;
let window2w = 20;
let window2h = 20;

function window2Draw() {
  ctx.fillStyle = "white";
  ctx.fillRect(window2x, window2y, window2w, window2h)
}

function window2Check(x, y) {
  if (x >= window2x && x <= window2x + window2w && y >= window2y && y <= window2y + window2h) {
    if (window2x == 40) {
      window2x = 200;
    }
    return 1;
  } else {
    return 0;
  }
}

function window2Move() {
  if (window2x != 40) {
    requestAnimationFrame(window2Move);
    ctx.clearRect(window2x, window2y, window2w, window2h);
    ctx.fillStyle = "white";
    ctx.fillRect(window2x, window2y, window2w, window2h)
    window2x -= 1;
  }
}


//grass
let grassx = 0;
let grassy = 100;
let grassw = 300;
let grassh = 40;

function grassDraw() {
  ctx.fillStyle = "green";
  ctx.fillRect(grassx, grassy, grassw, grassh)
}

function grassCheck(x, y) {
  if (x >= grassx && x <= grassx + grassw && y >= grassy && y <= grassy + grassh) {
    if (grassy == 260) {
      grassy = 100;
    }
    grassMove();
  }
}

function grassMove() {
  if (grassy != 260) {
    requestAnimationFrame(grassMove);
    ctx.clearRect(grassx, grassy, grassw, grassh);
    ctx.fillStyle = "green";
    ctx.fillRect(grassx, grassy, grassw, grassh);
    grassy += 1;
  }
}

//door
let doorx = 60;
let doory = 70;
let doorw = 20;
let doorh = 40;

function doorDraw() {
  ctx.fillStyle = "brown";
  ctx.fillRect(doorx, doory, doorw, doorh)
}

function doorCheck(x, y) {
  if (x >= doorx && x <= doorx + doorw && y >= doory && y <= doory + doorh && doory != 200) {
    if (doory == 220) {
      doory = 60;
    }
    return 1;
  } else {
    return 0;
  }
}

function doorMove() {
  if (doory != 220) {
    requestAnimationFrame(doorMove);
    ctx.clearRect(doorx, doory, doorw, doorh);
    ctx.fillRect(doorx, doory, doorw, doorh)
    ctx.fillStyle = "bown";
    doory += 1;
  }
}


//roof
let roofp1x = 150;
let roofp1y = 40;
let roofp2x = 270;
let roofp2y = 40;
let roofp3x = 210;
let roofp3y = 0;


function roofDraw() {
  ctx.fillStyle = "black";
  ctx.beginPath();
  ctx.moveTo(roofp1x, roofp1y);
  ctx.lineTo(roofp2x, roofp2y);
  ctx.lineTo(roofp3x, roofp3y);
  ctx.lineTo(roofp1x, roofp1y);
  ctx.closePath();
  ctx.fill();
}

function roofCheck(x, y) {
  if (x >= roofp1x && x <= roofp2x && y >= roofp3y && y <= roofp1y) {
    if (roofp1x == 10) {
      roofp1x = 150;
      roofp1y = 40;
      roofp2x = 270;
      roofp2y = 40;
      roofp3x = 210;
      roofp3y = 0;
    }
    roofMove();
  }
}

function roofMove() {
  if (roofp1x != 10 && roofp3y != 180) {
    requestAnimationFrame(roofMove)
    ctx.beginPath();
    ctx.moveTo(roofp1x, roofp1y);
    ctx.lineTo(roofp2x, roofp2y);
    ctx.lineTo(roofp3x, roofp3y);
    ctx.lineTo(roofp1x, roofp1y);
    ctx.closePath();
    ctx.fill();
    roofp1x -= 1;
    roofp1y += 1;
    roofp2x -= 1;
    roofp2y += 1;
    roofp3x -= 1;
    roofp3y += 1;
  }
}

function drawAll() {
  requestAnimationFrame(drawAll);
  drawBackground();
  houseDraw();
  window1Draw();
  window2Draw();
  grassDraw();
  doorDraw();
  roofDraw();
}

//Main
drawAll();
canvas.addEventListener("click", () => {
  const rect = canvas.getBoundingClientRect();
  setTimeout(1000);
  const x = event.clientX - rect.left;
  const y = event.clientY - rect.top;
  console.log(x, y);
  //Sjekker om figurene har blitt trykket på
  //if for å fjerne at man trykker på 2 figurer samtidig. på overlappende elementer
  //enklere enn å legge inn kordinatene i check funksjonen.
  if (window1Check(x, y) == 1) {
    window1Move();
  } else if (window2Check(x, y) == 1) {
    window2Move();
  } else if (doorCheck(x, y) == 1) {
    doorMove();
  } else if (houseCheck(x, y) == 1) {
    houseMove();
  }
  grassCheck(x, y);
  roofCheck(x, y);
  drawAll();
});
